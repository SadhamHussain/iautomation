global class IA1_ContactTriggerHandler {
  public static Map<Id, Set<Id>> integrationContactIdsMap = new Map<Id, Set<Id>>();
  
  public static void fetchIntegrationContactRecords(Map<Id, Contact> contactOldMap){
    System.debug('constructSobjectMergeEventRecords>>>> BEFORE TRIGGER');

    for(Integration_Contact__c thisIntegrationContact: [SELECT Id, Contact__c FROM Integration_Contact__c 
                                                                WHERE Contact__c IN: contactOldMap.keySet()]){
      if(!integrationContactIdsMap.containskey(thisIntegrationContact.Contact__c)){
        integrationContactIdsMap.put(thisIntegrationContact.Contact__c, new Set<Id>{});
      }     
      integrationContactIdsMap.get(thisIntegrationContact.Contact__c).add(thisIntegrationContact.Id);                                                
    }
  }
  
  public static void constructSobjectMergeEventRecords(Map<Id, Contact> contactOldMap){
    System.debug('constructSobjectMergeEventRecords>>>> AFTER TRIGGER');
    System.debug('constructSobjectMergeEventRecords>>>> AFTER TRIGGER' + integrationContactIdsMap);

    List<Sobject_Merge__e> sObjectMergeEventRecords = new List<Sobject_Merge__e>();
    for(Contact thisContact :contactOldMap.values()){
      if(thisContact.MasterRecordId != null
      && integrationContactIdsMap.containskey(thisContact.Id)){
        for(Id thisIntegrationContactId : integrationContactIdsMap.get(thisContact.Id)){
          sObjectMergeEventRecords.add(IA1_SobjectMergeService.constructSobjectMergeRecord(thisContact.MasterRecordId, thisIntegrationContactId));
        }
      }
    }
    if(!sObjectMergeEventRecords.isEmpty()){
      System.debug('constructSobjectMergeEventRecords>>> BEFORE PUBLISH');

      IA1_SobjectMergeService.publishEvents(sObjectMergeEventRecords);
    }
  }
}