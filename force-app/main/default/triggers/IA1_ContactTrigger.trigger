trigger IA1_ContactTrigger on Contact (before delete, after delete) {
  if (Trigger.isBefore ) {
    if (Trigger.isDelete) {
      IA1_ContactTriggerHandler.fetchIntegrationContactRecords(Trigger.oldMap);
    }
  }
  else {
    if(Trigger.isDelete) {
      IA1_ContactTriggerHandler.constructSobjectMergeEventRecords(Trigger.oldMap);
    }
  }
}