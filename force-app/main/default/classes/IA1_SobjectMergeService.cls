public without sharing class IA1_SobjectMergeService {
    public static Sobject_Merge__e constructSobjectMergeRecord(Id masterRecordId, Id childRecordId){
        Sobject_Merge__e thisMerge      = new Sobject_Merge__e();
        thisMerge.Child_Record_Id__c    = childRecordId;
        thisMerge.Master_Record_Id__c   = masterRecordId;
        thisMerge.Child_SobjectType__c  = String.valueOf(childRecordId.getSobjectType());
        return thisMerge;
    }

    public static void publishEvents(List<SObject> platformEvents) {
		System.debug(' SobjectMerge  >>  publishEvents >>> platformEvents '+ platformEvents);
		for (Database.SaveResult result : System.EventBus.publish(platformEvents)) {
			if (!result.isSuccess()) {
				for (Database.Error error : result.getErrors()) {
					System.debug(LoggingLevel.ERROR, error.getMessage());
				}
			}
		}
	}
}